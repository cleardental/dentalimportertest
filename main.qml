// Copyright 2023 Clear.Dental; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import QtQuick3D
import QtQuick3D.Helpers

Window {
    id: appWindow
    width: 1920
    height: 1080
    visible: true
    View3D {
        id: view
        anchors.fill: parent
//        environment.antialiasingMode: SceneEnvironment.SSAA
//        environment.antialiasingQuality: SceneEnvironment.VeryHigh
//        environment.aoEnabled: true


        Node {
            id: node0123

            // Resources
            PrincipledMaterial {
                id: material_001_material5678
                baseColor: "#ffcccccc"
                roughness: 0.15
                cullMode: Material.NoCulling

            }

            // Nodes:
            Node {
                id: rootNode1234
                PointLight {
                    id: light_light234567
                    position: Qt.vector3d(-2863.74, 222.752, 120.233)
                    rotation: Qt.quaternion(0.707107, -0.707107, 0, 0)
                    scale: Qt.vector3d(100, 100, 100)
                    brightness: 2
                    quadraticFade: 0.0022222246043384075
                }
                PerspectiveCamera {
                    id: camera_camera3456
                    position: Qt.vector3d(-2500, 139.477, 0)
                    rotation: Qt.quaternion(0.707107, 0, -0.707107, 0)
                    scale: Qt.vector3d(100, 100, 100)
                    fieldOfView: 45
                    fieldOfViewOrientation: PerspectiveCamera.Horizontal
                }
                Model {
                    id: upperJawScan4567
                    property real rotateMe: 0
                    //position: Qt.vector3d(0, 52.3366, 314.01)
                    position: Qt.vector3d(-289.061, 238.975, -28.818)
                    rotation: Qt.quaternion(0.584811, 0.397488, 0.397488, 0.584811)
                    eulerRotation.y: rotateMe
                    scale: Qt.vector3d(10.5703, 10.5703, 10.5703)
                    source: "qrc:/mesh/upper"
                    materials: [
                        material_001_material5678
                    ]

                    PropertyAnimation {
                        running: true
                        loops: Animation.Infinite
                        from: 0
                        to: 360
                        duration: 10000
                        target: upperJawScan4567
                        property: "rotateMe"
                    }
                }

                Node {
                    Model {
                        id: rect
                        source: "#Rectangle"
                        position: Qt.vector3d(500,0,0)
                        scale: Qt.vector3d(100,100,100);
                        eulerRotation.y: -45

                        materials: DefaultMaterial {
                            diffuseColor: "#aaaaffff"
                            cullMode: Material.NoCulling
                        }

                    }
                }
            }



            // Animations:
        }
    }

//    DebugView {
//        source: view
//        resourceDetailsVisible: true
//    }


}
